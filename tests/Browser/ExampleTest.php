<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TopLowNewTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testPages()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('високи')
                    ->assertSee('Ea ut eaque deleniti cumque quis. Reiciendis veniam maxime aut sit fuga quis ipsa. Et et non nihil ut.')

                    ->visit('/bg/top/page/6')
                    ->assertSee('Molestiae laboriosam voluptates ex voluptatibus et ipsum quae. Et consequuntur consequuntur perferendis non.')

                    ->visit('/bg/low/page/1')
                    ->assertSee('Molestiae laboriosam voluptates ex voluptatibus et ipsum quae. Et consequuntur consequuntur perferendis non.')

                    ->visit('/bg/low/page/6')
                    ->assertSee('Ea ut eaque deleniti cumque quis. Reiciendis veniam maxime aut sit fuga quis ipsa. Et et non nihil ut.')

                    ->visit('/bg/new/page/1')
                    ->assertSee('Ea ut eaque deleniti cumque quis. Reiciendis veniam maxime aut sit fuga quis ipsa. Et et non nihil ut.')

                    ->visit('/bg/new/page/6')
                    ->assertSee('Molestiae laboriosam voluptates ex voluptatibus et ipsum quae. Et consequuntur consequuntur perferendis non.')

                    ->visit('/en')
                    ->assertSee('top')
                    ->assertSee('4')
                    ->assertSee('Nesciunt consequatur et reiciendis vitae. Nihil optio veniam earum aliquam aut error.')
                    ->assertSee('3')
                    ->assertSee('Illum enim quia quis sit. Similique aliquam in provident vel. Aut nostrum ipsum saepe. Dolores eligendi sit officiis non.')
                    ->assertSee('Porro sit et perspiciatis tenetur mollitia mollitia praesentium. Ut perspiciatis aut quasi. At saepe quia molestiae.')

                    ->visit('/en/top/page/7')
                    ->assertSee('-2')
                    ->assertSee('Corporis quaerat rem est nisi. Quia ipsum consequuntur ex autem maiores aut. Fugiat id est ut nemo atque est voluptas.')
                    ->assertSee('Assumenda eius in aliquid eum. Et sed sed rerum aliquid atque autem. Nam et eum autem doloribus quam sed.')

                    ->visit('/en/low/page/1')
                    ->assertSee('-2')
                    ->assertSee('Corporis quaerat rem est nisi. Quia ipsum consequuntur ex autem maiores aut. Fugiat id est ut nemo atque est voluptas.')
                    ->assertSee('Assumenda eius in aliquid eum. Et sed sed rerum aliquid atque autem. Nam et eum autem doloribus quam sed.')

                    ->visit('/en/new/page/1')
                    ->assertSee('Porro sit et perspiciatis tenetur mollitia mollitia praesentium. Ut perspiciatis aut quasi. At saepe quia molestiae.')
                    ->assertSee('Esse adipisci earum eligendi voluptates fuga rerum maiores. Suscipit magni quaerat molestiae.')

                    ->visit('/en/new/page/7')
                    ->assertSee('Assumenda eius in aliquid eum. Et sed sed rerum aliquid atque autem. Nam et eum autem doloribus quam sed.')

                    ->visit('/en/category/technology/top/page/1')
                    ->assertSee('4')
                    ->assertSee('Nesciunt consequatur et reiciendis vitae. Nihil optio veniam earum aliquam aut error.')
                    ->assertSee('Porro sit et perspiciatis tenetur mollitia mollitia praesentium. Ut perspiciatis aut quasi. At saepe quia molestiae.')

                    ->visit('/en/category/technology/top/page/2')
                    ->assertSee('Ut sed doloribus ipsam. Voluptas accusantium labore vitae illum velit nisi consectetur. Autem aut et et pariatur veniam.')

                    ->visit('/en/category/technology/top/page/3')
                    ->assertSee('Page not found.')

                    ->visit('/bg/category/science/top/page/1')
                    ->assertSee('Ea ut eaque deleniti cumque quis. Reiciendis veniam maxime aut sit fuga quis ipsa. Et et non nihil ut.')
                    ->assertSee('Voluptatum autem nobis cumque. Modi iure voluptatem error molestiae.')

                    ->visit('/bg/category/politics/top/page/1')
                    ->assertSee('Quia aut tempore et sed quas id voluptas. Nobis recusandae ad natus dolor quasi. Sed non occaecati laborum tenetur.')

                    ->visit('/bg/category/politics/top/page/2')
                    ->assertSee('Non quod accusantium et. Placeat hic sequi incidunt quia ducimus. Voluptates itaque eos ducimus cupiditate.')

                    ->visit('/bg/category/politics/top/page/3')
                    ->assertSee('Не намерихме страницата, която търсите.')

                    ->visit('/bg/category/politics/low/page/1')
                    ->assertSee('Non quod accusantium et. Placeat hic sequi incidunt quia ducimus. Voluptates itaque eos ducimus cupiditate.')

                    ->visit('/bg/category/politics/low/page/2')
                    ->assertSee('Quia aut tempore et sed quas id voluptas. Nobis recusandae ad natus dolor quasi. Sed non occaecati laborum tenetur.')

            ;
        });
    }
}
