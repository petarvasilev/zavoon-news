# Zavoon News 

A news aggregator powering the [Zavoon News](https://news.zavoon.com) website.

## Getting started

1. Give write permission to /bootstrap and /storage with

        chmod -R 777 storage
    
2. Create .env file off the .env.example and populate it.

3. Install dependencies

        composer install

4. Run the migrations

        php artisan migrate
    
5. If on production populate database with necessary data

        php artisan db:seed --class=ProductionSeeder
    
if developing add dummy data with
    
        php artisan db:seed --class=DatabaseSeeder
        
## License

Source code licensed under MIT (below). Logo is private & copyrighted.

Copyright 2020 [Petar Vasilev](https://www.petarvasilev.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

