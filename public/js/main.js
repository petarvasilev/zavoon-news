var Main = function()
{
    var self = this;

    self.init = function () {
        self.events();
    };

    self.vote = function (userVote, linkID, element)
    {

        $.post('/vote/add/' + linkID + '/' + userVote, {_token: $('input[name=_token]').val()}, function (data) {
            if (data.status == 'OK')
            {
                var userVoteDigit = userVote == 'up' ? 1 : -1;
                var linkVotes = $(element).parent().parent().parent().find('.link-votes');

                var newLinkVote = parseInt(linkVotes.html()) + userVoteDigit;
                linkVotes.html(newLinkVote);

                $(element).addClass('active-vote');
            }
        });
    };

    self.voteRemove = function (userVote, linkID, element)
    {
        $.post('/vote/remove/' + linkID, {_token: $('input[name=_token]').val()}, function (data) {
            if (data.status == 'OK')
            {
                var userVoteDigit = userVote == 'up' ? -1 : 1;
                var linkVotes = $(element).parent().parent().parent().find('.link-votes');

                var newLinkVote = parseInt(linkVotes.html()) + userVoteDigit;
                linkVotes.html(newLinkVote);

                $(element).removeClass('active-vote');
            }
        });
    };

    self.events = function () {
        // on voting
        $(document).on('click', '.vote-up, .vote-down', function ()
        {
            var vote = $(this).attr('data-vote');
            var linkID = $(this).attr('data-link-id');

            if ($('.not-logged-in').length == 1)
            {
                window.location = '/' + Laravel.locale + '/login?error=voting'
            }
            if ($(this).hasClass('active-vote'))
            {
                self.voteRemove(vote, linkID,  this)
            }
            else
            {
                self.vote(vote, linkID, this);
            }
        });

        // on selecting a category
        $(document).on('change', '#select-category', function()
        {
            var category_slug = $(this).val();
            var request_uri = window.location.href.split(Laravel.app_url)[1];
            var localized_url = self.localizeUrl(request_uri);
            var categorized_url = self.addCategoryToUrl(localized_url, category_slug);

            window.location = categorized_url;
        });

        $(document).on('click', '#mobile-menu-button', function()
        {
            var menu = $('#app-navbar-collapse');

            if ($(this).hasClass('active'))
            {
                menu.hide();
                $(this).removeClass('active');
            }
            else
            {
                menu.show();
                $(this).addClass('active');
            }
        });
    };

    /**
     * Adds a category to a url
     *
     * @param url
     * @param category
     * @returns {*}
     */
    self.addCategoryToUrl = function (url, category)
    {
        var urlCategory = self.matchCategoryUrl(url);

        if (urlCategory && category !== 'all')
        {
            return url.replace(urlCategory['slug'], category);
        }
        else if (urlCategory && category == 'all')
        {
            return url.replace('\/category\/' + urlCategory['slug'], '');
        }
        else if (!urlCategory && category !== 'all')
        {
            if (self.isLocaleInUrl(url))
            {
                return url.replace('/' + Laravel.locale, '/' + Laravel.locale + '/category/' + category)
            }
            else
            {
                return '/category/' + category + url;
            }
        }
        else
        {
            if (console)
            {
                console.log('There was problem adding the category to the url.');
            }
        }
    };

    /**
     * Checks if current locale is in a url
     *
     * @param url
     * @returns {boolean}
     */
    self.isLocaleInUrl = function(url)
    {
        if (url.indexOf('/' + Laravel.locale) == 0)
        {
            return true;
        }

        return false;
    };

    self.matchCategoryUrl = function(url)
    {
        for (var i = 0; i < Laravel.categories.length; i++)
        {
            if (url.indexOf('/category/' + Laravel.categories[i]['slug']) !== -1)
            {
                return Laravel.categories[i];
            }
        }

        return false;
    };

    /**
     * Adds current locale to a url if it's not localized already
     *
     * @param url
     * @returns {*}
     */
    self.localizeUrl = function (url)
    {
        if (url.indexOf('/' + Laravel.locale) == 0)
        {
            return url;
        }

        return '/' + Laravel.locale + url;
    };

    return self;
};