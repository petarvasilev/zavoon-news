<?php

use App\Language;
use Illuminate\Database\Seeder;
use App\Category;
use App\Link;
use App\User;
use App\Vote;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create languages
        Language::create([
            'name' => 'Български',
            'slug' => 'bulgarian',
            'short_name' => 'bg'
        ]);

        Language::create([
            'name' => 'English',
            'slug' => 'english',
            'short_name' => 'en'
        ]);

        // create categories
        Category::create([
            'name' => 'Технологии',
            'english_name' => 'Technology',
            'slug' => 'technology'
        ]);

        Category::create([
            'name' => 'Наука',
            'english_name' => 'Science',
            'slug' => 'science'
        ]);

        Category::create([
            'name' => 'Политика',
            'english_name' => 'Politics',
            'slug' => 'politics'
        ]);

        Category::create([
            'name' => 'Други',
            'english_name' => 'Other',
            'slug' => 'other'
        ]);


    }
}
