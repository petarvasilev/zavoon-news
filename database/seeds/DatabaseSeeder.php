<?php

use App\Language;
use Illuminate\Database\Seeder;
use App\Category;
use App\Link;
use App\User;
use App\Vote;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');
        $faker->seed(123);

        $currentDatetime = date('Y-m-d H:i:s', time());

        // create users
        for ($i = 0; $i < 5; $i++)
        {
            User::create([
                'name' => $faker->unique()->name,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('secret'),
                'remember_token' => str_random(10),
            ]);
        }

        // create links
        for ($i = 0; $i < 119; $i++)
        {
            $faker->seed($i);

            Link::create([
                'title' => $faker->text(124),
                'url' => $faker->url,
                'description' => $faker->text(512),
                'user_id' => $faker->numberBetween(1, 5),
                'language_id' => $faker->numberBetween(1, 2),
                'category_id' => $faker->numberBetween(1, 4),
                'created_at' => $currentDatetime,
                'updated_at' => $currentDatetime
            ]);
        }

        // create one link with created_at two seconds in the future
        $faker->seed(120);
        Link::create([
            'title' => $faker->text(124),
            'url' => $faker->url,
            'description' => $faker->text(512),
            'user_id' => $faker->numberBetween(1, 5),
            'language_id' => 2,
            'category_id' => $faker->numberBetween(1, 4),
            'created_at' => date('Y-m-d H:i:s', time() + 2),
            'updated_at' => $currentDatetime
        ]);

        // create votes
        for ($i = 0; $i < 120; $i++)
        {
            $faker->seed($i);

            Vote::create([
                'user_id' => $faker->numberBetween(1, 5),
                'link_id' => $faker->unique()->numberBetween(1, 120),
                'vote' => 1
            ]);
        }

        // create 3 upvotes for 1 specific link
        for ($i = 0; $i < 3; $i++)
        {
            $faker->seed($i);

            Vote::create([
                'user_id' => $faker->numberBetween(1, 5),
                'link_id' => 60,
                'vote' => 1
            ]);
        }

        // create 2 upvotes for 1 specific link
        for ($i = 0; $i < 2; $i++)
        {
            $faker->seed($i);

            Vote::create([
                'user_id' => $faker->numberBetween(1, 5),
                'link_id' => 61,
                'vote' => 1
            ]);
        }

        // create 3 downvotes for 1 specific link
        for ($i = 0; $i < 3; $i++)
        {
            $faker->seed($i);

            Vote::create([
                'user_id' => $faker->numberBetween(1, 5),
                'link_id' => 62,
                'vote' => -1
            ]);
        }

        // create languages
        Language::create([
            'name' => 'Български',
            'slug' => 'bulgarian',
            'short_name' => 'bg'
        ]);

        Language::create([
            'name' => 'English',
            'slug' => 'english',
            'short_name' => 'en'
        ]);

        // create categories
        Category::create([
            'name' => 'Технологии',
            'english_name' => 'Technology',
            'slug' => 'technology'
        ]);

        Category::create([
            'name' => 'Наука',
            'english_name' => 'Science',
            'slug' => 'science'
        ]);

        Category::create([
            'name' => 'Политика',
            'english_name' => 'Politics',
            'slug' => 'politics'
        ]);

        Category::create([
            'name' => 'Други',
            'english_name' => 'Other',
            'slug' => 'other'
        ]);


    }
}
