<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = ['name', 'slug', 'short_name'];

    public function links()
    {
        return $this->hasMany('App\Link');
    }
}
