<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Session;

class RegistrationController extends Controller
{
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            Session::flash('flash-error', __('text.confirmation-code-invalid'));
            return view('auth.confirm');
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            Session::flash('flash-error', __('text.confirmation-code-invalid'));
            return view('auth.confirm');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        Session::flash('flash', __('text.email-confirmed'));

        return view('auth.confirm');
    }
}
