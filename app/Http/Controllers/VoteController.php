<?php

namespace App\Http\Controllers;

use App\Link;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function vote($linkID, $userVote)
    {
        if ($userVote == 'up')
        {
            $userVote = 1;
        }
        else
        {
            $userVote = -1;
        }

        $user = Auth::user();

        $vote = (new Vote())->where('link_id', $linkID)->where('user_id', $user->id)->first();

        if (count($vote) == 0)
        {
            $newVote = new Vote();
            $newVote->user_id = $user->id;
            $newVote->link_id = $linkID;
            $newVote->vote = $userVote;

            if ($newVote->save())
            {
                return ['status' => 'OK', 'msg' => 'Гласувахте успешно.'];
            }
        }
        else
        {
            return ['status' => 'BAD', 'msg', 'msg' => 'Вие вече сте гласували за този линк.'];
        }
    }

    /**
     * Removes a vote for a particular link
     *
     * @param $linkID integer - the link's id for which the vote needs to be removed
     * @return array
     */
    public function voteRemove($linkID)
    {
        $user = Auth::user();

        // search for a link with the same id as the one to be removed
        // and add by the current user
        $link = (new Link)->where('id', $linkID)->where('user_id', $user->id)->first();

        if ($link)
        {
            // if the above specified link is found return an error
            // as users are not allowed to remove votes on their own links
            return ['status' => 'BAD', 'msg' => 'Не можете да изтриете вота към линк който вие сте довавили.'];
        }

        $vote = (new Vote())->where('link_id', $linkID)->where('user_id', $user->id)->first();

        if ($vote)
        {
            if ($vote->delete())
            {
                return ['status' => 'OK', 'msg' => 'Вотът беше премахнат успешено.'];
            }

            return ['status' => 'BAD', 'Неуспешно изтриване.'];
        }

        return ['status' => 'BAD', 'Не открихме вотът ви'];
    }
}
