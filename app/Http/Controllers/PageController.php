<?php

namespace App\Http\Controllers;

use App\Category;
use App\Link;
use App\Logic\Pages\Titles;
use App\Logic\Pagination\Pagination;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class PageController extends Controller
{
    /**
     * Show the home page
     *
     * @return \Illuminate\View\View
     */
    public function home()
    {
        return $this->topLinks();
    }

    /**
     * Renders a view with the top links displayed first unless
     * $reverse is passed in as true in which case the lowest
     * links are first
     *
     * @param int $page the page number
     * @param bool $reverse false if you want top links true if you want low links
     * @param bool $category the category id
     * @return \Illuminate\View\View
     */
    public function links($page = 1, $reverse = false, $category = false)
    {
        $page = (int)$page;

        if (!Pagination::isValidPageNumber($page)) { // validate page number
            return response(['Invalid page.']);
        }

        $offset = Pagination::calculateOffset($page);

        $links = Link::top((int)env('LINKS_PER_PAGE'), $offset, $reverse, $category);

        $user = Auth::user();

        if ($user)
        {
            $links = Link::votify($links, $user);
        }

        list($pageTitle, $metaTitle, $metaDescription) = Titles::topAndLow($page, $reverse);

        $pagesNumber = Link::getNumberOfPages($category);

        // if requested page doesn't exist return a view to notify the user
        if ($page > $pagesNumber && $page > 1)
        {
            return view('errors.404');
        }

        config('app.locale');

        return view('pages.home')
            ->with('links', $links)
            ->with('pagesNumber', $pagesNumber)
            ->with('currentPage', $page)
            ->with('paginationUrl', $reverse ? 'low' : 'top')
            ->with('paginationStartsAtPage', Pagination::calculateFirstPageNumber($pagesNumber, $page))
            ->with('linksType', $reverse ? 'low' : 'top')
            ->with('categories', Category::all())
            ->with('currentCategory', $category)
            ->with('pageTitle', $pageTitle)
            ->with('metaTitle', $metaTitle)
            ->with('metaDescription', $metaDescription);
    }

    /**
     * Renders a view with the top links displayed first unless
     * $reverse is passed in as true in which case the lowest
     * link are first
     *
     * @param int $page
     * @return \Illuminate\View\View
     */
    public function topLinks($page = 1)
    {
        return $this->links($page);
    }

    /**
     * This calls
     *
     * @param int $page
     * @return Controller|\Illuminate\View\View
     */
    public function lowLinks($page = 1)
    {
        return $this->links($page, true);
    }

    /**
     * Renders a view with the newest links displayed
     *
     * @param int $page the page number
     * @param bool $category false for no category integer for an existing category
     * @return \Illuminate\View\View
     */
    public function newLinks($page = 1, $category = false)
    {
        $page = (int)$page;

        if (!Pagination::isValidPageNumber($page)) { // validate page number
            return response(['Invalid page.']);
        }

        $offset = Pagination::calculateOffset($page);

        $links = Link::newest((int)env('LINKS_PER_PAGE'), $offset, $category);

        $user = Auth::user();

        if ($user)
        {
            $links = Link::votify($links, $user);
        }

        $pagesNumber = Link::getNumberOfPages($category);

        // if requested page doesn't exist return a view to notify the user
        if ($page > $pagesNumber && $page > 1)
        {
            return view('errors.404');
        }

        return view('pages.home')
            ->with('links', $links)
            ->with('pagesNumber', $pagesNumber)
            ->with('currentPage', $page)
            ->with('paginationUrl', 'new')
            ->with('paginationStartsAtPage', Pagination::calculateFirstPageNumber($pagesNumber, $page))
            ->with('linksType', 'new')
            ->with('categories', Category::all())
            ->with('currentCategory', $category)
            ->with('pageTitle', 'Най-новите новини')
            ->with('metaTitle', 'Zavoon News - Най-новите новини')
            ->with('metaDescription', 'Най-новите новини от потребителите ни.');
    }

    /**
     * Renders the view for individual links
     *
     * @param $linkID
     * @return \Illuminate\View\View
     */
    public function linkView($linkID)
    {
        $link = Link::find($linkID);

        if ($link)
        {
            return view('pages.link-view')
                ->with('link', $link)
                ->with('pageTitle', $link['title'])
                ->with('metaTitle', $link['title'])
                ->with('metaDescription', $link['description']);
        }

        abort(404);
    }

    public function categoryTop($category, $page = 1)
    {
        $category = Category::where('slug', '=', strtolower($category))->first();

        if (!$category)
        {
            return view('errors.404');
        }

        return $this->links($page, false, $category);
    }

    public function categoryLow($category, $page = 1)
    {
        $category = Category::where('slug', '=', strtolower($category))->first();

        if (!$category)
        {
            return view('errors.404');
        }

        return $this->links($page, true, $category);
    }

    public function categoryNew($category, $page = 1)
    {
        $category = Category::where('slug', '=', strtolower($category))->first();

        if (!$category)
        {
            return view('errors.404');
        }

        return $this->newLinks($page, $category);
    }
}
