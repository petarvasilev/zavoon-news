<?php

namespace App\Http\Controllers;

use App\Category;
use App\Language;
use App\Link;
use App\Vote;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user's profile page
     *
     * @return View
     */
    public function profile()
    {
        $user = Auth::user();

        return view('user.profile')->with('user', $user);
    }

    /**
     * Saves the profile information
     *
     * @param Request $request
     * @return Redirector
     */
    public function profileSave(Request $request)
    {
        $newName = $request->input('name');
        $user = Auth::user();

        $user->name = $newName;

        if ($user->save())
        {
            Session::flash('flash', __('notifications.your-profile-was-saved'));
        }

        return back();
    }

    /**
     * Displays the view adding new links
     *
     * @return View
     */
    public function linkAdd()
    {
        return view('user.link-add')
            ->with('categories', Category::all())
            ->with('languages', Language::all());
    }

    /**
     * Saves new links into the database
     *
     * @param Request $request
     * @return Redirector
     */
    public function linkAddSave(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:512',
            'url' => 'required|active_url|max:512',
            'category_id' => 'required|exists:categories,id',
            'language_id' => 'required|exists:languages,id',
            'description' => 'string|max:512',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);

        $user = Auth::user();
        $input = $request->all();
        $input['user_id'] = $user->id;

        $link = new Link($input);

        if ($link->save())
        {
            $vote = new Vote();
            $vote->user_id = $user->id;
            $vote->link_id = $link->id;
            $vote->vote = 1;
            $vote->save();

            Session::flash('flash', __('notifications.successfully-added-link'));

            return redirect('/' . config('app.locale') .'/link/details/' . $link->id);
        }
    }

    /**
     * Displays all links added by the currently logged in user
     *
     * @param Request $request
     * @return View
     */
    public function userLinks(Request $request)
    {
        $user = Auth::user();

        $links = (new Link())
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        foreach ($links as $key => $link)
        {
            $category = Category::where('id', '=', $link['category_id'])->first();
            $language = Language::where('id', '=', $link['language_id'])->first();

            if (config('app.locale') == 'bg')
            {
                $links[$key]['category'] = $category->name;
            }
            else
            {
                $links[$key]['category'] = $category->english_name;
            }
            $links[$key]['language'] = $language->name;
        }

        return view('user.user-links')
            ->with('links', $links)
            ->with('startAt', ($links->currentPage() -1) * 10 + 1);
    }

    /**
     * Displays details of a submitted link
     *
     * @param $linkID
     * @return View
     */
    public function linkDetails($linkID)
    {
        $user = Auth::user();
        $link = (new Link())->where('id', $linkID)->where('user_id', $user->id)->first();


        return view('user.link-details')
            ->with('link', $link)
            ->with('categories', Category::all())
            ->with('languages', Language::all());
    }
}
