<?php

namespace App\Logic;

use App\Language;

class Localization {

    /**
     * Returns a language based on the current language
     *
     * @return mixed
     */
    static function getCurrentLanguage()
    {
        $locale = config('app.locale');

        return Language::where('short_name', '=', $locale)->first();
    }

    static function getLocalizedEnglishUrl()
    {
        $request_uri = $_SERVER['REQUEST_URI'];

        if (strpos($request_uri, '/en') === 0)
        {
            return $request_uri;
        }
        elseif (strpos($request_uri, '/bg') === 0)
        {
            return str_replace('/bg', '/en', $request_uri);
        }

        return '/en' . $request_uri;
    }

    static function getLocalizedBulgarianUrl()
    {
        $request_uri = $_SERVER['REQUEST_URI'];

        if (strpos($request_uri, '/bg') === 0)
        {
            return $request_uri;
        }
        elseif (strpos($request_uri, '/en') === 0)
        {
            return str_replace('/en', '/bg', $request_uri);
        }

        return '/bg' . $request_uri;
    }

}