<?php

namespace App\Logic\Pages;

class Titles {
    /**
     * Returns page title, meta title and meta description for the
     * top and low pages
     *
     * @param $page
     * @param $reverse
     * @return array
     */
    static function topAndLow($page, $reverse)
    {
        if ($page == 1 && !$reverse)
        {
            $pageTitle = __('text.homepage-title');
            $metaTitle = 'Zavoon News - ' . __('text.homepage-title');
            $metaDescription = __('text.homepage-meta');
        }
        elseif ($page == 1 && $reverse)
        {
            $pageTitle = __('text.low-new-title');
            $metaTitle = 'Zavoon News - ' . $pageTitle;
            $metaDescription = __('text.low-news-meta');
        }
        elseif($reverse)
        {
            $pageTitle = __('text.low-news-title') . ' - ' . __('text.page') . ' ' . $page;
            $metaTitle = 'Zavoon News - ' . $pageTitle;
            $metaDescription = __('text.low-news-meta') . ' ' . __('text.page') . ' '  . $page . '.';
        }
        elseif( ! $reverse)
        {
            $pageTitle = __('text.top-news-title') . ' - ' . __('text.page') . ' ' . $page;
            $metaTitle = 'Zavoon News - ' . $pageTitle;
            $metaDescription = __('text.top-news-meta') . ' ' . __('text.page') . ' '  . $page . '.';
        }

        return [$pageTitle, $metaTitle, $metaDescription];
    }

}