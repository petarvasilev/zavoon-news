<?php

namespace App\Logic;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordCustom extends ResetPassword
{
    public function toMail($notifiable)
    {
        if (config('app.locale') == 'bg')
        {
            return (new MailMessage)
                ->subject('Промяна на паролата')
                ->greeting('Здравейте')
                ->line('Получавате този имейл понеже някой поиска промяна на паролата за вашият акаунт.')
                ->action('Променете паролата', url(config('app.url') . route('password.reset', $this->token, false)))
                ->line('Ако не сте поискали промяна на паролата просто игнорирайте този имейл.');
        }
        else
        {
            return (new MailMessage)
                ->subject('Password Reset')
                ->line('You are receiving this email because we received a password reset request for your account.')
                ->action('Reset Password', url(config('app.url') . route('password.reset', $this->token, false)))
                ->line('If you did not request a password reset, no further action is required.');
        }
    }
}