<?php

namespace App\Logic\Pagination;

class Pagination {

    /**
     * Given a page number it calculates what the offset needs
     * to be for the SQL query
     *
     * @param $page
     * @return int
     */
    static function calculateOffset($page)
    {
        $limit = (int)env('LINKS_PER_PAGE');

        if (preg_match('/^[0-9]+$/', $page))
        {
            $offset = ($page - 1)*$limit;
        }
        else
        {
            $offset = 0;
        }

        return $offset;
    }

    /**
     * This calculates what is the first page showed in the pagination
     * e.g if the current page is 5 this will return 3 which is the
     * first pagination page link displayed
     *
     * @param $pagesNumber
     * @param $currentPage
     * @return int
     */
    static function calculateFirstPageNumber($pagesNumber, $currentPage)
    {
        $paginationStartsAtPage = 1;

        if ($currentPage > 3)
        {
            $paginationStartsAtPage = $currentPage - 2;
        }

        if ($currentPage == $pagesNumber OR $currentPage == $pagesNumber -1)
        {
            $paginationStartsAtPage = $pagesNumber - 4;
        }

        if ($paginationStartsAtPage < 1)
        {
            $paginationStartsAtPage = 1;
        }

        return $paginationStartsAtPage;
    }

    /**
     * Validates page number essentially has to be number
     *
     * @param $page
     * @return bool
     */
    static function isValidPageNumber($page)
    {
        if (preg_match('/^[0-9]+$/', $page)) { // validate page number
            return true;
        }

        return false;
    }

}