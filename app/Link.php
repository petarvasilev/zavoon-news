<?php

namespace App;

use App\Logic\Localization;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class Link extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'description', 'user_id', 'language_id', 'category_id'
    ];

    /**
     * Gets the top links, this function decides which links
     * go on the first page and which on the others
     *
     * @param $limit int
     * @param $offset int
     * @param $reversed bool
     * @param $category bool|int if valid category id get results only from that category
     * @return mixed
     */
    static function top($limit, $offset, $reversed, $category)
    {
        $order = 'DESC'; // most up voted first

        if ($reversed)
        {
            $order = 'ASC'; // most down voted first
        }

        $language = Localization::getCurrentLanguage();

        if ($category)
        {
            $query = 'SELECT links.id, links.title, links.url, links.user_id, links.language_id, links.category_id,  links.created_at,
                  sum(vote) as votes,
                  (sum(vote)) / POWER(time_to_sec(TIMEDIFF(now(),links.created_at))/3600, 1.8) as `rank`
                  FROM links
                  INNER JOIN votes ON links.id = votes.link_id 
                  WHERE links.language_id = ' . $language->id . ' AND links.category_id = ' . $category->id . '
                  GROUP BY links.id
                  ORDER BY `rank` ' . $order . ', links.id ' . $order . ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }
        else
        {
            $query = 'SELECT links.id, links.title, links.url, links.user_id, links.language_id, links.category_id, links.created_at, 
                  sum(vote) as votes,
                  (sum(vote)) / POWER(time_to_sec(TIMEDIFF(now(),links.created_at))/3600, 1.8) as `rank`
                  FROM links
                  INNER JOIN votes ON links.id = votes.link_id
                  WHERE links.language_id = ' . $language->id . '
                  GROUP BY links.id
                  ORDER BY `rank` ' . $order . ', links.id ' . $order . ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        return DB::select(DB::raw($query));
    }

    /**
     * Get newest links
     *
     * @param $limit
     * @param $offset
     * @return mixed
     */
    static function newest($limit, $offset, $category = false)
    {
        $language = Localization::getCurrentLanguage();

        if ($category)
        {
            $query = 'SELECT links.id, links.title, links.url, links.user_id, links.language_id, links.category_id,  links.created_at, 
                  sum(vote) as votes
                  FROM links
                  INNER JOIN votes ON links.id = votes.link_id
                  WHERE links.language_id = ' . $language->id . ' AND links.category_id = ' . $category->id . '
                  GROUP BY links.id 
                  ORDER BY links.created_at DESC, links.id DESC LIMIT ' . $limit . ' OFFSET ' . $offset;
        }
        else
        {
            $query = 'SELECT links.id, links.title, links.url, links.user_id, links.language_id, links.category_id,  links.created_at, 
                  sum(vote) as votes
                  FROM links
                  INNER JOIN votes ON links.id = votes.link_id
                  WHERE links.language_id = ' . $language->id . '
                  GROUP BY links.id 
                  ORDER BY links.created_at DESC, links.id DESC LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        return DB::select(DB::raw($query));
    }

    /**
     * Given an array of links and a user it checks if the user has voted
     * on each individual links and sets some data in the link data so
     * that this can be displayed, for example, on the front end
     *
     * @param $links
     * @param $user
     * @return mixed
     */
    static function votify($links, $user)
    {
        for($i = 0; $i < count($links); $i++)
        {
            $userVote = (new Vote())
                ->where('link_id', '=', $links[$i]->id)
                ->where('user_id', '=', $user->id)->first();

            if ($userVote)
            {
                $links[$i]->userVote = $userVote->vote;
            }
            else
            {
                $links[$i]->userVote = false;
            }
        }

        return $links;
    }

    /**
     * Returns the number of pages based on how many links are there in the
     * database and how many links are displayed per page
     *
     * @param bool|int $category integer if counting post from certain category only
     * @return float
     */
    static function getNumberOfPages($category = false)
    {
        $language = Localization::getCurrentLanguage();

        if ($category)
        {
            return ceil(DB::table('links')
                    ->where('language_id', '=', $language->id)
                    ->where('category_id', '=', $category->id)
                    ->count() / (int)env('LINKS_PER_PAGE')); // count only from certain categories
        }
        else
        {
            return ceil(DB::table('links')
                    ->where('language_id', '=', $language->id)
                    ->count() / (int)env('LINKS_PER_PAGE')); // count all links
        }
    }

    /**
     * Defines a belongTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Defines a hasMany relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    /**
     * Defines a belongTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    /**
     * Defines a belongTo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
