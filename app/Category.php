<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'english_name', 'slug'];

    public function links()
    {
        return $this->hasMany('App\Link');
    }
}
