<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    // home page same as top page 1
    Route::get('/', 'PageController@home');

    // results
    Route::get('/top/page/{page}', 'PageController@topLinks');
    Route::get('/low/page/{page}', 'PageController@lowLinks');
    Route::get('/new/page/{page}', 'PageController@newLinks');

    // category pages
    Route::get('/category/{category}', 'PageController@categoryTop');
    Route::get('/category/{category}/top/page/{page}', 'PageController@categoryTop');
    Route::get('/category/{category}/low/page/{page}', 'PageController@categoryLow');
    Route::get('/category/{category}/low/page/{page}', 'PageController@categoryLow');
    Route::get('/category/{category}/new/page/{page}', 'PageController@categoryNew');
    Route::get('/category/{category}/new/page/{page}', 'PageController@categoryNew');

    // profile
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile/save', 'HomeController@profileSave');

    // form for adding and saving new links
    Route::get('/link/add', 'HomeController@linkAdd');
    Route::post('/link/add/save', 'HomeController@linkAddSave');

    // user viewing his own links and link details
    Route::get('/user/links', 'HomeController@userLinks');
    Route::get('/link/details/{linkID}', 'HomeController@linkDetails');

    // all users seeing the link details
    Route::get('/link/view/{linkID}', 'PageController@linkView');

    // voting
    Route::post('/vote/add/{linkID}/{vote}', 'VoteController@vote');
    Route::post('/vote/remove/{linkID}/', 'VoteController@voteRemove');

    // email confirmation
    Route::get('/register/verify/{confirmationCode}', 'RegistrationController@confirm');

    // registration, login etc.
    Auth::routes();
});

