@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('text.your-links') }}</div>

                    <div class="panel-body">

                        @if (count($links) > 0)
                            <table class="table table-stripped no-padding-top">
                                <tbody>
                                    @foreach($links as $link)

                                        <tr>
                                            <td>{{ $startAt }}</td>
                                            <td>
                                                <div><a href="/{{ config('app.locale') }}/link/details/{{ $link['id'] }}">{{ $link['title'] }}</a></div>

                                                <div class="details">
                                                    {{ mb_strtolower(__('text.added-on')) }}: {{ $link['created_at'] }},
                                                    {{ mb_strtolower(__('text.category')) }}: {{ mb_strtolower($link['category']) }},
                                                    {{ mb_strtolower(__('text.language')) }}: {{ mb_strtolower($link['language']) }}
                                                </div>
                                            </td>
                                        </tr>

                                        @php $startAt++; @endphp
                                    @endforeach
                                </tbody>
                            </table>

                            {{ $links->links() }}
                        @else
                            <h3 style="margin-top: 5px;">Вие нямате добавени линкове.</h3>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
