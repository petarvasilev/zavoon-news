@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('text.add-link') }}</div>

                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ '/' . config('app.locale') }}/link/add/save">
                        {{ csrf_field() }}

                        @if (Session::has('flash'))
                            <div class="alert alert-success">{{ Session::get('flash') }}</div>
                        @endif

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">{{ __('text.title') }} *</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" autofocus>

                                @if ($errors->has('title'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url" class="col-md-4 control-label">{{ __('text.link') }} *</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control" name="url">

                                @if ($errors->has('url'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="category_id" class="col-md-4 control-label">{{ __('text.category') }} *</label>

                            <div class="col-md-6">
                                <select name="category_id" id="category_id" class="form-control">
                                    @foreach($categories as $category)
                                        @if(config('app.locale') == 'bg')
                                            <option value="{{ $category->id }}">
                                                {{ $category->name }}
                                            </option>
                                        @else
                                            <option value="{{ $category->id }}">
                                                {{ $category->english_name }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('language_id') ? ' has-error' : '' }}">
                            <label for="language_id" class="col-md-4 control-label">{{ __('text.language') }} *</label>

                            <div class="col-md-6">
                                <select name="language_id" id="language_id" class="form-control">
                                    @foreach($languages as $language)
                                        <option @if(config('app.locale') == 'en' && $language->id == 2){{ 'selected' }}@endif value="{{ $language->id }}">
                                            {{ $language->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @if ($errors->has('language_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('language_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">{{ __('text.description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control" name="description"></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                {!! Recaptcha::render(['size' => 'compact']) !!}

                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('text.add') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
