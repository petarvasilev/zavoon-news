@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('text.profile') }}</div>

                <div class="panel-body">

                    <form class="form-horizontal" role="form" method="POST" action="{{ '/' . config('app.locale') }}/profile/save">
                        {{ csrf_field() }}

                        @if (Session::has('flash'))
                             <div class="alert alert-success">{{ Session::get('flash') }}</div>
                        @endif

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ __('text.name') }}</label>

                            <div class="col-md-6">
                                <input value="{{ $user->name }}" id="name" type="text" class="form-control" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ __('text.email-address') }}</label>

                            <div class="col-md-6">
                                <input  disabled id="email" type="email" class="form-control" name="email" value="{{ $user->email  }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('text.save') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
