@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('text.your-link') }}</div>

                    <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST">
                            {{ csrf_field() }}

                            @if (Session::has('flash'))
                                <div class="alert alert-success">{{ Session::get('flash') }}</div>
                            @endif

                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">{{ __('text.title') }}</label>

                                <div class="col-md-6">
                                    <input disabled value="{{ $link['title'] }}" id="title" type="text" class="form-control" name="title" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="url" class="col-md-4 control-label">{{ __('text.link') }}</label>

                                <div class="col-md-6">
                                    <input disabled value="{{ $link['url'] }}" id="url" type="text" class="form-control" name="url" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="category" class="col-md-4 control-label">{{ __('text.category') }}</label>

                                <div class="col-md-6">
                                    <select name="category" id="category" class="form-control" disabled>
                                        @foreach($categories as $category)
                                            @if($category->id == $link['category_id'])
                                                @if(config('app.locale') == 'bg')
                                                    <option selected>{{ $category->name }}</option>
                                                @else
                                                    <option selected>{{ $category->english_name }}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="language" class="col-md-4 control-label">{{ __('text.language') }}</label>

                                <div class="col-md-6">
                                    <select name="language" id="language" class="form-control" disabled>
                                        @foreach($languages as $language)
                                            @if($language->id == $link['language_id'])
                                                <option selected>{{ $language->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-md-4 control-label">{{ __('text.description') }}</label>

                                <div class="col-md-6">
                                    <textarea disabled id="description" class="form-control" name="description" required>{{ $link['description'] }}</textarea>

                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
