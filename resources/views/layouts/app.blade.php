<!DOCTYPE html>
<html lang="bg">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ url('/favicon.png') }}">

    @if (isset($metaTitle))
        <meta name="title" content="{{ $metaTitle }}">
        <meta name="og:title" content="{{ $metaTitle }}">
    @endif

    @if (isset($metaDescription) && trim($metaDescription) != '')
        <meta name="description" content="{{ $metaDescription }}">
        <meta name="og:description" content="{{ $metaDescription }}">
    @endif

    <title>Zavoon News @if (isset($pageTitle) && $pageTitle) {{ ' - ' . $pageTitle }} @else {{ ' - Агрегатор на новини' }} @endif</title>

    <!-- Styles -->
    <link href="/css/main.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'locale' => config('app.locale'),
            'app_url' => env('APP_URL'),
            'categories' => \App\Category::all()
        ]); ?>
    </script>

    @if (env('APP_ENV') == 'production')
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-30101732-20"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-30101732-20');
        </script>


        <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u="//piwik.zavoon.com/";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
        </script>
        <!-- End Piwik Code -->
    @endif

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="/{{ config('app.locale') }}">
                        <img src="{{ url('/img/zavoon.png') }}" alt="">
                    </a>

                    <div id="mobile-menu-button">
                        <div class="top"></div>
                        <div class="mid"></div>
                        <div class="low"></div>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="/{{ config('app.locale') }}/login">{{ __('text.log-in') }}</a></li>
                            <li><a class="not-logged-in" href="/{{ config('app.locale') }}/register">{{ __('text.sign-up') }}</a></li>
                            @include('partials.lang-dropdown')
                        @else
                            <li>
                                <a href="/{{ config('app.locale') }}/profile">
                                    {{ __('text.profile') }}
                                </a>
                            </li>

                            <li>
                                <a href="/{{ config('app.locale') }}/link/add">
                                    {{ __('text.add-link') }}
                                </a>
                            </li>

                            <li>
                                <a href="/{{ config('app.locale') }}/user/links">
                                    {{ __('text.your-links') }}
                                </a>
                            </li>

                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/{{ config('app.locale') }}/logout"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('text.logout') }}
                                        </a>

                                        <form id="logout-form" action="/{{ config('app.locale') }}/logout" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>

                            @include('partials.lang-dropdown')
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

        <footer class="panel-footer">
            <div id="copyright">
                Zavoon &copy; {{ date('Y') }}
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="/js/lib/jquery.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>

    <script>
        $(function () {
            var main = new Main();
            main.init();
        })
    </script>

    @yield('script')
</body>
</html>
