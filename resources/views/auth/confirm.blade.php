@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div id="home-page-heading" class="panel-heading">
                        {{ __('text.email-confirmation') }}
                    </div>
                    <div class="panel-body display-links-panel">

                        @if (Session::has('flash'))
                            <div class="alert alert-success">{{ Session::get('flash') }}</div>
                        @endif

                        @if (Session::has('flash-error'))
                            <div class="alert alert-danger">{{ Session::get('flash-error') }}</div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
