<div id="home-page-heading" class="panel-heading">
    <a href="/{{ config('app.locale') }}/@if($currentCategory){{ 'category/' . $currentCategory->slug . '/' }}@endif{{ 'top' }}/page/1">{{ __('text.top') }}</a>
    <span>|</span>
    <a href="/{{ config('app.locale') }}/@if($currentCategory){{ 'category/' . $currentCategory->slug . '/' }}@endif{{ 'low' }}/page/1">{{ __('text.low') }}</a>
    <span>|</span>
    <a href="/{{ config('app.locale') }}/@if($currentCategory){{ 'category/' . $currentCategory->slug . '/' }}@endif{{ 'new' }}/page/1">{{ __('text.new') }}</a>

    @if(count($categories) > 0)
        <select name="cateogry" id="select-category">
            <option value="all">
                @if(config('app.locale') == 'bg')
                    Всички
                @else
                    All
                @endif
            </option>
            @foreach($categories as $category)
                <option @if($currentCategory && $category->id == $currentCategory->id){{ 'selected' }}@endif value="{{ $category->slug }}">
                    @if(config('app.locale') == 'bg')
                        {{ $category->name }}
                    @else
                        {{ $category->english_name }}
                    @endif
                </option>
            @endforeach
        </select>
    @endif
</div>
<div class="panel-body display-links-panel">
    {{ csrf_field() }}

    <table class="links-table table table-responsive">
        <tbody>
            @if(count($links) == 0)
                <tr>
                    <td style="padding: 15px 0 0 0; text-align: center;">{{  __('text.В момента няма дабавени линкове') }}</td>
                </tr>
            @else
                @foreach ($links as $link)
                    <tr>
                        <td>
                            <div class="voting">
                                <div data-link-id="{{ $link->id }}" data-vote="up" class="vote-up @if (isset($link->userVote) and $link->userVote == 1) {{ 'active-vote' }} @endif">
                                    <span class="glyphicon glyphicon-circle-arrow-up"></span></div>
                                <div data-link-id="{{ $link->id }}" data-vote="down" class="vote-down @if (isset($link->userVote) and $link->userVote == -1) {{ 'active-vote' }} @endif">
                                    <span class="glyphicon glyphicon-circle-arrow-down"></span></div>
                            </div>
                        </td>
                        <td>
                            <div class="link-votes">{{ $link->votes }}</div>
                        </td>
                        <td>
                            <div><a href="{{ $link->url }}" target="_blank">{{ $link->title }}</a></div>
                            <div>
                                <span>{{ substr($link->created_at, 0, 16) }}</span>,

                                <span>
                                    {{ strtolower(__('text.category-lowercase')) }}:

                                    @foreach ($categories as $category)
                                        @if ($category->id == $link->category_id)
                                            @if(config('app.locale') == 'bg')
                                                {{ mb_strtolower($category->name) }},
                                            @else
                                                {{ strtolower($category->english_name) }},
                                            @endif
                                        @endif
                                    @endforeach
                                </span>
                                <span>
                                    <a href="/{{ config('app.locale') }}/link/view/{{ $link->id }}">{{ __('text.comments') }}</a>
                                </span>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    @include('partials.pagination')

</div>