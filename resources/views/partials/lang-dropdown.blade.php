
<li class="dropdown">

    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Lang <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{{ \App\Logic\Localization::getLocalizedEnglishUrl() }}">
                English
            </a>
        </li>
        <li>
            <a href="{{ \App\Logic\Localization::getLocalizedBulgarianUrl() }}">
                Български
            </a>
        </li>
    </ul>
</li>