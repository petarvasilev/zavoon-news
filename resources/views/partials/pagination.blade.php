@if($pagesNumber > 1)
    <div id="pagination">
        <div class="model-pagination">
            <ul class="pagination">
                @if($currentPage != 1)
                    <li>
                        <a href="/{{ config('app.locale') }}@if($currentCategory){{ '/category/' . $currentCategory->slug }}@endif{{ '/' . $paginationUrl . '/page/' . ($currentPage - 1) }}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                @endif

                @for ($i = $paginationStartsAtPage; $i < $paginationStartsAtPage + 5 && $i <= $pagesNumber; $i++)
                    <li class="{{ $i == $currentPage ? 'active' : '' }}"><a href="/{{ config('app.locale') }}@if($currentCategory){{ '/category/' . $currentCategory->slug }}@endif{{ '/' . $paginationUrl . '/page/' . $i }}">{{ $i }}</a></li>
                @endfor

                @if($currentPage != $pagesNumber && $pagesNumber > 2)
                    <li>
                        <a href="/{{ config('app.locale') }}@if($currentCategory){{ '/category/' . $currentCategory->slug }}@endif{{ '/' . $paginationUrl . '/page/' . ($currentPage + 1) }}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
@endif