@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <p style="text-align: center; margin-top: 11px;">Не намерихме линкове, който да ви покажем.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
