<?php

return [

    'your-profile-was-saved' => 'Your profile was saved.',
    'successfully-added-link' => 'The link was added successfully.',
    'before-voting' => 'Before you can vote you need to login.'

];