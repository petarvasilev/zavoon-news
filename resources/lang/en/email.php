<?php

return [
    'hello' => 'Hello',
    'first-copy' => 'Please confirm your e-mail address',
    'greetings' => 'Greetings',
    'verify-email-subject' => 'Confirm your e-mail',
    'confirm-email-action' => 'Confirm e-mail'
];