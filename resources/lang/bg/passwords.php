<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролата трябва да е дълга поне 6 букви и да е еднаква с повторението.',
    'reset' => 'Паролата ви бе променена.',
    'sent' => 'Изпратихме ви имейл чрез който може да промените паролата си.',
    'token' => 'Токена за промяна на парола е навалиден.',
    'user' => "Не може да намерим потребил с такъв имейл адрес.",

];
