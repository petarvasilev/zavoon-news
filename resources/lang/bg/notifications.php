<?php

return [

    'your-profile-was-saved' => 'Профилът ви е запаметен.',
    'successfully-added-link' => 'Успешно добавихте нов линк.',
    'before-voting' => 'Преди да гласувате трябва да влезете в акаунта си.'

];