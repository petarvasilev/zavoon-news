<?php

return [
    'hello' => 'Здравей',
    'first-copy' => 'Моля потвърдете имейл адреса ви',
    'greetings' => 'Поздрави',
    'verify-email-subject' => 'Потвърди имейла си',
    'confirm-email-action' => 'Потвърди имейл'
];